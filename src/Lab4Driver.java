import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Lab #4 CS 2334, Section 0?? September 26, 2013
 * <P>
 * This class implements a program that tests the Paper and Author classes.
 * </P>
 * 
 * @author
 * @version 1.0
 */
public class Lab4Driver {
	
	@SuppressWarnings("unchecked")
	public static List<Paper> readPapers(String filename) throws ClassNotFoundException, IOException {
	
		FileInputStream fileInputStream = new FileInputStream(filename);
		ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
		
		List<Paper> papers = (List<Paper>) objectInputStream.readObject();
		
		objectInputStream.close();
		return papers;
	}
	
	/**
	 * This is the main method for this test program. Since this is a simple
	 * test program, all of our code will be in the main method. Typically this
	 * would be a bad design, but we are just testing out some features of Java.
	 * <P>
	 * 
	 * @param args
	 *            Contains the command line arguments.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		
		// --- Step 8 ---
		Author author1 = new Author( "Garrison", "John A.", "University of Oklahoma" );
		Author author2 = new Author( "Sidwell", "Nicholas S.", "College of Engineering" );
		Author author3 = new Author( "Lucas", "George W.", "Lucas Films" );
		
		ArrayList<Author> authorList = new ArrayList<Author>();
		authorList.add( author1 );
		authorList.add( author2 );
		authorList.add( author3 );

		Paper paper1 = new Paper( "On The Topic of Prophylactics", authorList, 2013 );
		
		Paper.writePaper( "Papers.txt", paper1 );

		paper1 = null;
		System.out.println( paper1 );
		paper1 = Paper.readPaper( "Papers.txt" );
		System.out.println( paper1 );

		// --- Step 11 ---
		List<Paper> papers = new ArrayList<Paper>();
		
		papers.add(new Paper("On the Translation of Languages from Left to Right", 
				new ArrayList<Author>(Arrays.asList(new Author("Lucas", "Luca", "Luca Institute"), 
						new Author("Johnson", "Joe", "Joestitute"), new Author("Brostein", "Albert", 
								"Institute of Bro"))), 1978));
	
		papers.add(new Paper("On the Translation of Languages from Right to Left", 
				new ArrayList<Author>(Arrays.asList(new Author("Bronson", "Carl", "Albertsons"), 
						new Author("Monster", "Cookie", "SS Institute"), new Author("Frankson", "Brett", 
								"Institute of Torus-Shaped Donuts"))), 1945));
		
		papers.add(new Paper("On the Tesselation of Languages from Left to Right", 
				new ArrayList<Author>(Arrays.asList(new Author("Smithson", "Smithson", "Smithstitute"), 
						new Author("PhD", "Dr", "Brain School"), new Author("What", "Say", 
								"School for the Deaf"))), 1921));
		
		papers.add(new Paper("On the Tesselation of Languages from Right to Left", 
				new ArrayList<Author>(Arrays.asList(new Author("Yankovic", "Weird Al", "University of Tulsa"), 
						new Author("Bran", "Raisin", "Kellogg's Research Campus"), new Author("Jackson",
								"John", "Michael's Pre-school"))), 1987));
		
		papers.add(new Paper("Right to Left: From Languages of Translation", 
				new ArrayList<Author>(Arrays.asList(new Author("Brown", "Sweet", "Oh Lawd Jesus It's a Fur"), 
						new Author("Jones", "DeAnte", "Call, Fold, All In"), new Author("Is Dead", "Flash", 
								"Apple Inc."))), 2000));
		
		papers.add(new Paper("Left to Right: From Languages of Translation", 
				new ArrayList<Author>(Arrays.asList(new Author("Sidwell", "Nicholas", "University of Oklahoma"), 
						new Author("Garrison", "John", "University of Oklahoma"), new Author("Hougen", "Dean", 
								"University of Oklahoma"))), 2013));
		
		java.util.Collections.sort(papers);
		Lab4Driver.writePapers("Written Paper List.txt", papers);
		
		papers.clear();
		System.out.println(papers.toString());
		
		papers = Lab4Driver.readPapers("Written Paper List.txt");
		System.out.println(papers.toString());
		
		System.out.println(Searcher.binarySearch(papers,
				new Paper("On the Translation of Languages from Left to Right",
						null, 1994)));
	}
	
	public static void writePapers( String filename, List<Paper> papers ) throws IOException
	{
		FileOutputStream fileOutputStream = new FileOutputStream( filename );
		ObjectOutputStream objectOutputStream = new ObjectOutputStream( fileOutputStream );
		objectOutputStream.writeObject( papers );
		objectOutputStream.close();
	}
}